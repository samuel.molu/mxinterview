package fr.mx.scc.entities;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class SccEntity {

    @Id
    private String clientId;
    private String cardIdPub;
    private String cardIdPriv;
}
