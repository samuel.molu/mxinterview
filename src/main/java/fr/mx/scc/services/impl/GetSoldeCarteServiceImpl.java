package fr.mx.scc.services.impl;

import java.nio.charset.StandardCharsets;
import java.util.Optional;

import org.springframework.stereotype.Service;

import fr.mx.scc.entities.SccEntity;
import fr.mx.scc.repositories.SccRepository;
import fr.mx.scc.services.GetSoldeCarteService;
import fr.mx.scsc.services.ScscRequestService;
import fr.mx.sic.beans.CardDetails;
import fr.mx.sic.services.SicRequestService;
import fr.mx.ssc.services.SscRequestService;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class GetSoldeCarteServiceImpl implements GetSoldeCarteService {

    private final SicRequestService sicService;
    private final SccRepository sccRepository;
    private final SscRequestService sscService;
    private final ScscRequestService scscService;

    @Override
    public double getSolde(final String clientId, final String publicCardId) {
        //récupérer l'identifiant privé de la carte à partir de l'identifiant du client et de la cle public
        final Optional<SccEntity> sccEntity= sccRepository.findById(clientId);
        //récupérer le detail de la carte encrypté
        final CardDetails cardDetails= sicService.getCardDetails(sccEntity.get().getCardIdPriv());
        //récupérer la clé de cryptage du client à partir de son identifiant
        final String key = sscService.getKey(clientId);
        //décrypter les informations de la carte
        final String cardNumber = sscService.decryptCard(key, cardDetails.getNumero().getBytes(StandardCharsets.UTF_8));
        //récupérer le solde de la carte à partir du numero en clair de la carte
        final double solde= scscService.getSolde(cardNumber);
        //retourner le solde
        return solde;
    }

}
