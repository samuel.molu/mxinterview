package fr.mx.scc.services;

public interface GetSoldeCarteService {

    /**
     * Retourne le solde d'une carte pour un client donnée et une carte donnée
     * @param clientId
     * @param publicCardId
     * @return le montant du solde de la carte
     */
    double getSolde(String clientId, String publicCardId);


}
