package fr.mx.scc.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.mx.scc.entities.SccEntity;

public interface SccRepository extends JpaRepository<SccEntity, String> {

}
