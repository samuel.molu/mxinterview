package fr.mx.ssc.services;

public interface SscRequestService {

    String getKey(String clientId);

    String decryptCard(String key, byte[] base64encryptedCardNumber);


}
