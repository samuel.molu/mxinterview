package fr.mx.sic.services;

import fr.mx.sic.beans.CardDetails;

public interface SicRequestService {

    CardDetails getCardDetails(String cardPrivateId);

}
