package fr.mx.sic.beans;

import java.util.Date;

import lombok.Data;

@Data
public class CardDetails {

    private String numero;
    private String ccv;
    private Date validite;
}
