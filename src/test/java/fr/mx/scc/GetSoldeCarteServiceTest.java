package fr.mx.scc;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import fr.mx.scc.services.impl.GetSoldeCarteServiceImpl;
import fr.mx.sic.beans.CardDetails;
import fr.mx.sic.services.SicRequestService;

@ExtendWith(MockitoExtension.class)
class GetSoldeCarteServiceTest {

    @Mock
    SicRequestService sicService;

    @InjectMocks
    GetSoldeCarteServiceImpl serviceUnderTest;


    @BeforeEach
    public void setup() {
        final CardDetails card = new CardDetails();
        card.setCcv("XXX");
        card.setNumero("5467XXXX89");
        Mockito.when(sicService.getCardDetails(Mockito.any())).thenReturn(card);

    }

    @Test
    void shouldCallGetSolde() {

        final String clientId = "MX_CLIENT_ID";
        final String publicCardId = "CARD_ID_PUB";

        final double solde = serviceUnderTest.getSolde(clientId , publicCardId );

        Assertions.assertEquals(100, solde);

    }

}
