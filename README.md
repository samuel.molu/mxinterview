# mxinterview

Cet exercice a pour but

- d’évaluer les connaissances que vous avez par rapport au besoin de la mission à laquelle vous êtes destiné
- de vous préparez aux problématiques auxquelles vous allez faire face.

## Exercice


La société **MX** émet des cartes de paiement à destination de ses clients. Un client de MX peut récupérer le solde d’une carte qui a été émise pour lui. Pour obtenir cette information, le client utilise une API publique mise à disposition et accessible par internet : le Service de Consultation des soldes des Cartes (SCC).

Une carte possède un identifiant public fourni au client et un identifiant privé utilisé en interne par MX, connu seulement de MX. De plus le client possède un identifiant unique.

Le SCC stocke la relation entre l’identifiant du client, la clé publique de la carte et l’identifiant interne de la carte dans une base de données MySQL. Pour calculer le solde de la carte. le SCC utilise les 3 services internes suivants :

- Service des Informations sur les cartes (SIC)
Ce service stocke le détail des cartes par client, par type de carte, ainsi que l’identifiant privé de la carte. Ce service expose une API permettant à partir de la clé privée de la carte d’obtenir le détail de la carte (numéro, date de péremption, ccv) sous forme encryptée.

- Service de Calcul Solde de la Carte (SCSC)
Ce service calcul le solde de la carte à partir des historiques de paiements. Ce service expose une API qui retourne le solde de la carte à partir du numéro de carte en clair

- Service de Sécurité et Cryptage (SSC) :
Ce service est chargé de la gestion des clés de cryptage. Ce service expose une API permettant de retourner la clé de cryptage pour un client donné.

Le développeur du service à commencer à écrire des tests unitaires, mais il n’a pas terminé. MX à mis en place des métriques d’intégration continue, il faut obtenir une couverture d’au moins 80% de la méthode. Pouvez-vous s’il vous plaît terminer, faire passer le test et assurer la couverture de code demandé ?

## Entretien d’évaluation technique


#### Gestion des dépendances : Maven

- Installer 

Comment on installe Maven sous Windows ?

- Configurer 

Où se trouvent les dépendances télécharger ? 

Comment peut-on changer ce répertoire ? 

Le repository central est interne, il faut se connecter avec un user/mdp pour télécharger les dépendances. Comment configure-t-on ces informations ?

Utiliser Comment on lance les tests d’un projet avec Maven ? 

Comment on génère un livrable ? 

Où se trouve le livrable généré ?

#### Environnement Intégré de Développement : Eclipse ou IntelliJ

- Installer 

Comment installer Eclipse ou IntelliJ sous Windows ?

- Configurer 

Sais-tu ajouter un plugin sous Eclipse / IntelliJ ? 
Sais-tu importer un projet existant sur le disque ? 
Sais-tu importer un projet depuis un gestionnaire de configuration ?

- Utiliser

Sais-tu configurer et lancer une application avec des paramètres spécifiques à la VM depuis Eclipse / intelliJ ?

#### Gestion de versions : git & svn

- Installer

Comment installer Git sous Windows ?

- Configurer : 

Sais-tu configurer git pour ajouter un utilisateur ? 
Pour ajouter un éditeur de texte ?

- Utiliser : 

Sais-tu copier un projet distant sous git en local ?

Sais-tu modifier les projets et mettre à jour le projet local sous git ? le projet distant ? 

Branching : Sais-tu créer une branche ? 

Merging Sais-tu résoudre un conflit avec une autre branche ? 

Rebase Sais-tu ajouter ton code depuis ta branche vers la branche principale ?

Git flow tu connais git flow ?

#### Développer en Java 8 : Savoir Coder, Refactoriser et Tester

- Reconnaître Composition, Héritage, Encapsulation, Inversion de contrôle, 
- Programmation par Interface, Composition vs Extension, 
- Utiliser les Collections et les lambdas
- Lire, comprendre et modifier du code existant


#### Les Framework Java et l’écosystème Monext

- Junit 4 vs Junit 5, Mockito

Différence entre Test unitaire vs Test d’intégration vs Test e2e (Selenium)

- Spring framework: Configuration Java vs XML, IOC, MVC

Identifier dans le projet les annotations relatives à Spring Framework, Spring Boot, Spring MVC

Identifier un cas d’injection de dépendances

Comment feriez-vous pour ajouter un API REST pour le service SCC qui retourne le solde de la carte sur l’URI suivante 
`/clients/{clientId}/cards/{cardId}/solde` ?

Que pensez-vous de cet URI ?
Respecte-t-elle les principes d’une URI REST ? 
Quelle librairie manque au projet pour ajouter un Contrôleur ?

Il n’existe pas de Carte correspondant à l’identifiant fourni par le client, le service de détail des cartes lève une exception 
`CardNotFoundException extends Exception`

De quel type d’exception s’agit-il ? 

Même question si `CardNotFoundException extends RuntimeException` ? 

Comment convertir cette Exception en message d'erreur HTTP avec Spring MVC ?

#### Spring data JPA, Spring JDBC, Hibernate ORM
	
Comment implémenteriez vous la recherche de l’identifiant privé de la carte à partir de l’identifiant client et l’identifiant public de la carte ?


Pouvez-vous écrire la définition de la table pour le service SCC de l’exercice en SQL ?

POuvez-vous configurer une connexion dans l’application à la base MySQL ?

#### Intégration Continue

- Gitlab : Savez-vous ce qu’est une Merge Request ou Pull Request ?

#### Divers

Linux : Comment se connecter à un environnement Linux à partir de Windows ? Notion de SSH, Client SSH ?

